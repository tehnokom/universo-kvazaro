# Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!

Workers of the world, unite!

Пролетарии всех стран, соединяйтесь!

# Основная информация

Это репозиторий Веб-клиента реального виртуального мира Универсо (Universo) на Quasar Framework

Смотрите список активных Обсуждений (Issues) здесь в репозитории https://gitlab.com/tehnokom/universo-kvazaro/-/issues

Более подробная информация в главном репозитории Универсо вот тут по ссылке https://gitlab.com/tehnokom/universo

Так же смотрите репозиторий с задачами по наполнению Универсо и сбору предложений

https://gitlab.com/tehnokom/universo-esplorado

Наш дискорд-сервер по Универсо https://discord.gg/bsvdPy5

Репозиторий клиентского приложения Реального виртуального мира Универсо (Godot Engine)

https://gitlab.com/tehnokom/universo

Хотите поддержать проект? Вам сюда https://boosty.to/universo

# Tehnokom App (tehnokom-app)

Tehnokom web-application

## Install the dependencies

```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Lint the files

```bash
npm run lint
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).

### Proxy for backend

https://gitlab.com/RA9OAJ/nginx-dev-proxy
