import gql from 'graphql-tag'
// запрос для получения списка пользователей
export const uzantojQuery = gql`
  query Uzantoj($first: Int = 1, $after: String, $serchi: String) {
    lasto: uzantoj(last: 1) {
      pageInfo {
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          objId
          id
        }
      }
    }
    uzantoj(
      first: $first
      after: $after
      isActive: true
      konfirmita: true
      serchi: $serchi
      orderBy: ["-krea_dato"]
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          id
          objId
          uuid
          unuaNomo {
            enhavo
          }
          duaNomo {
            enhavo
          }
          familinomo {
            enhavo
          }
          sekso
          statistiko {
            miaGekamarado
            miaGekamaradoPeto
            kandidatoGekamarado
            tutaGekamaradoj
            rating
            aktivaDato
          }
          isActive
          konfirmita
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          statuso {
            enhavo
          }
          kontaktaInformo
        }
      }
    }
  }
`
// запрос для получения данных пользователя по objID
export const uzantoByObjIdQuery = gql`
  query Uzanto($id: Float!) {
    uzanto: uzantoj(objId: $id) {
      edges {
        node {
          id
          objId
          uuid
          unuaNomo {
            enhavo
          }
          duaNomo {
            enhavo
          }
          familinomo {
            enhavo
          }
          sekso
          statistiko {
            miaGekamarado
            miaGekamaradoPeto
            kandidatoGekamarado
            tutaGekamaradoj
            rating
            aktivaDato
          }
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          statuso {
            enhavo
          }
          kontaktaInformo
        }
      }
    }
  }
`
// запрос для получения списка товарищей
export const gekamaradojQuery = gql`
  query Gekamaradoj($first: Int = 1, $after: String, $serchi: String) {
    lasto: gekamaradoj(last: 1) {
      pageInfo {
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          objId
          id
        }
      }
    }
    gekamaradoj(
      first: $first
      after: $after
      isActive: true
      konfirmita: true
      serchi: $serchi
      orderBy: ["-krea_dato"]
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          id
          objId
          uuid
          unuaNomo {
            enhavo
          }
          duaNomo {
            enhavo
          }
          familinomo {
            enhavo
          }
          sekso
          statistiko {
            miaGekamarado
            miaGekamaradoPeto
            kandidatoGekamarado
            tutaGekamaradoj
            rating
            aktivaDato
          }
          isActive
          konfirmita
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          statuso {
            enhavo
          }
          kontaktaInformo
        }
      }
    }
  }
`
// Запрос для получения списка всех сообществ
export const komunumojQuery = gql`
  query Komunumoj(
    $first: Int = 1
    $after: String
    $enDato: DateTime
    $serchi: String
    $tipo: String
    $forigo: Boolean = false
  ) {
    lasto: komunumoj(last: 1, tipo_Kodo: $tipo) {
      pageInfo {
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          objId
          id
        }
      }
    }
    komunumoj(
      first: $first
      after: $after
      enDato: $enDato
      serchi: $serchi
      tipo_Kodo_In: $tipo
      orderBy: ["-rating", "-aktiva_dato"]
      forigo: $forigo
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          id
          objId
          uuid
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          statistiko {
            postulita
            tuta
            mia
            membraTipo
          }
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          kovrilo {
            bildoE {
              url
            }
          }
        }
      }
    }
  }
`
// Запрос для получения данных конкретного сообщества по objID
export const komunumoQuery = gql`
  query Komunumo($id: Float) {
    komunumo: komunumoj(objId: $id) {
      edges {
        node {
          id
          objId
          uuid
          tipo {
            kodo
            nomo {
              enhavo
            }
          }
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          statistiko {
            postulita
            tuta
            mia
            membraTipo
          }
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          kovrilo {
            bildoE {
              url
            }
          }
          informo {
            enhavo
          }
          informoBildo {
            bildo
            bildoMaks
          }
          kontaktuloj {
            edges {
              node {
                objId
                unuaNomo {
                  enhavo
                }
                duaNomo {
                  enhavo
                }
                familinomo {
                  enhavo
                }
                avataro {
                  bildoF {
                    url
                  }
                }
                kontaktaInformo
              }
            }
          }
          rajtoj
        }
      }
    }
  }
`
// запрос для получения списка проектов сообщества по ID сообщества
export const projektojQuery = gql`
  query Projektoj(
    $komunumoId: Float
    $first: Int = 25
    $forigo: Boolean = false
    $arkivo: Boolean = false
    $after: String
  ) {
    projektoj: projektojProjekto(
      first: $first
      projektojprojektoposedanto_PosedantoKomunumo_Id: $komunumoId
      arkivo: $arkivo
      forigo: $forigo
      after: $after
      orderBy: ["-krea_dato"]
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          id
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          tipo {
            nomo {
              enhavo
            }
          }
          pozicio
          realeco {
            nomo {
              enhavo
            }
          }
          kategorio {
            edges {
              node {
                nomo {
                  enhavo
                }
              }
            }
          }
          statuso {
            nomo {
              enhavo
            }
          }
          objekto {
            nomo {
              enhavo
            }
          }
          projektojprojektoposedantoSet {
            edges {
              node {
                projekto {
                  nomo {
                    enhavo
                  }
                }
              }
            }
          }
          tasko {
            edges {
              node {
                id
                #                nomo {
                #                  enhavo
                #                }
                #                priskribo {
                #                  enhavo
                #                }
              }
            }
          }
        }
      }
    }
  }
`
// запрос для получения данных проекта по UUID. После реализации в бакенде поменяем на ID
export const projektoByUuidQuery = gql`
  query Projekto($uuid: UUID, $objId: Float) {
    projekto: projektojProjekto(uuid: $uuid, objId: $objId) {
      edges {
        node {
          uuid
          id
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          tipo {
            nomo {
              enhavo
            }
          }
          pozicio
          realeco {
            nomo {
              enhavo
            }
          }
          kategorio {
            edges {
              node {
                nomo {
                  enhavo
                }
              }
            }
          }
          statuso {
            nomo {
              enhavo
            }
          }
          objekto {
            nomo {
              enhavo
            }
          }
          projektojprojektoposedantoSet {
            edges {
              node {
                projekto {
                  nomo {
                    enhavo
                  }
                }
              }
            }
          }
          tasko {
            edges {
              node {
                uuid
                id
                objId
                nomo {
                  enhavo
                }
                priskribo {
                  enhavo
                }
                kategorio {
                  edges {
                    node {
                      objId
                      nomo {
                        enhavo
                      }
                    }
                  }
                }
                tipo {
                  objId
                  nomo {
                    enhavo
                  }
                }
                statuso {
                  objId
                  nomo {
                    enhavo
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
// типы сообществ
export const komunumojTipojQuery = gql`
  query komunumojTipoj {
    komunumojTipoj {
      edges {
        node {
          nomo {
            enhavo
          }
          kodo
          kvantoKomomunumoj
        }
      }
    }
  }
`
// категории проектов
export const projektojProjektoKategorioQuery = gql`
  query projektojProjektoKategorio {
    kategorio: projektojProjektoKategorio {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`
// типы проектов
export const projektojProjektoTipoQuery = gql`
  query projektojProjektoTipoQuery {
    tipo: projektojProjektoTipo {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`
// статусы проектов
export const projektojProjektoStatusoQuery = gql`
  query projektojProjektoStatusoQuery {
    statuso: projektojProjektoStatuso {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`
// информация о текущем пользователе
export const mi = gql`
  query Mi {
    mi {
      id
      objId
      uuid
      unuaNomo {
        enhavo
      }
      duaNomo {
        enhavo
      }
      familinomo {
        enhavo
      }
      sekso
      konfirmita
      isActive
      chefaLingvo {
        id
        nomo
      }
      chefaTelefonanumero
      chefaRetposhto
      naskighdato
      loghlando {
        nomo {
          lingvo
          enhavo
          chefaVarianto
        }
        id
      }
      loghregiono {
        id
        nomo {
          enhavo
        }
      }
      agordoj
      avataro {
        id
        bildoE {
          url
        }
        bildoF {
          url
        }
      }
      statuso {
        enhavo
      }
      statistiko {
        miaGekamarado
        miaGekamaradoPeto
        kandidatoGekamarado
        tutaGekamaradoj
        rating
        aktivaDato
      }
      kontaktaInformo
      kandidatojTuta
      gekamaradojTuta
      chefaOrganizo {
        id
        nomo {
          enhavo
        }
      }
      sovetoj {
        edges {
          node {
            id
            nomo {
              enhavo
            }
          }
        }
      }
      sindikatoj {
        edges {
          node {
            id
            nomo {
              enhavo
            }
          }
        }
      }
      administritajKomunumoj {
        edges {
          node {
            id
            nomo {
              enhavo
            }
          }
        }
      }
      universoUzanto {
        id
        uuid
        retnomo
      }
      isAdmin
    }
  }
`
// запрос для получения списка всех проектов
export const tutaProjektojQuery = gql`
  query tutaProjektoj(
    $first: Int = 25
    $after: String
    $forigo: Boolean = false
    $arkivo: Boolean = false
  ) {
    tutaProjektoj: projektojProjekto(
      first: $first
      after: $after
      arkivo: $arkivo
      forigo: $forigo
      orderBy: ["-krea_dato"]
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          id
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          tipo {
            nomo {
              enhavo
            }
          }
          pozicio
          realeco {
            nomo {
              enhavo
            }
          }
          kategorio {
            edges {
              node {
                nomo {
                  enhavo
                }
              }
            }
          }
          statuso {
            nomo {
              enhavo
            }
          }
          objekto {
            nomo {
              enhavo
            }
          }
          projektojprojektoposedantoSet {
            edges {
              node {
                projekto {
                  nomo {
                    enhavo
                  }
                }
              }
            }
          }
          tasko {
            edges {
              node {
                objId
                uuid
                id
                nomo {
                  enhavo
                }
                priskribo {
                  enhavo
                }
              }
            }
          }
        }
      }
    }
  }
`
// типы задач
export const projektojTaskoTipoQuery = gql`
  query projektojTaskoTipoQuery {
    tipo: projektojTaskoTipo {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`
// категории задач
export const projektojTaskoKategorioQuery = gql`
  query projektojTaskoKategorioQuery {
    kategorio: projektojTaskoKategorio {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`
// статусы задач
export const projektojTaskoStatusoQuery = gql`
  query projektojTaskoStatusoQuery {
    statuso: projektojTaskoStatuso {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`
// запрос на получение данных задачи
export const projektojTaskoNode = gql`
  query projektojTaskoNode(
    $uuid: UUID
    $arkivo: Boolean = false
    $forigo: Boolean = false
    $objId: Float
  ) {
    projektojTasko(uuid: $uuid, arkivo: $arkivo, forigo: $forigo, objId: $objId) {
      edges {
        node {
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          uuid
          kategorio {
            edges {
              node {
                objId
                id
                nomo {
                  enhavo
                }
              }
            }
          }
          statuso {
            objId
            id
            nomo {
              enhavo
            }
          }
          tipo {
            objId
            id
            nomo {
              enhavo
            }
          }
          forigo
          forigaDato
          arkivo
          arkivaDato
        }
      }
    }
  }
`
// запрос связей проектов
export const projektojProjektoLigilo = gql`
  query ligoj(
    $first: Int
    $after: String
    $forigo: Boolean = false
    $arkivo: Boolean = false
    $ligilo_Uuid: UUID
    $posedanto_Uuid: UUID
    $publikigo: Boolean = true
  ) {
    posedanto: projektojProjektoLigilo(
      first: $first
      ligilo_Uuid: $ligilo_Uuid
      after: $after
      arkivo: $arkivo
      forigo: $forigo
      publikigo: $publikigo
    ) {
      edges {
        node {
          uuid
          posedanto {
            nomo {
              enhavo
            }
            priskribo {
              enhavo
            }
            uuid
            objId
          }
          uuid
          tipo {
            nomo {
              enhavo
            }
            uuid
            objId
          }
        }
      }
    }
    ligilo: projektojProjektoLigilo(
      first: $first
      posedanto_Uuid: $posedanto_Uuid
      after: $after
      arkivo: $arkivo
      forigo: $forigo
      publikigo: $publikigo
    ) {
      edges {
        node {
          uuid
          ligilo {
            nomo {
              enhavo
            }
            priskribo {
              enhavo
            }
            uuid
            objId
          }
          uuid
          tipo {
            nomo {
              enhavo
            }
            uuid
            objId
          }
        }
      }
    }
  }
`
// типы связей проектов
export const ligojTipoj = gql`
  query ligojTipoj {
    projektojProjektoLigiloTipo {
      edges {
        node {
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          objId
        }
      }
    }
  }
`
// запрос для получения упрощенного списка проектов сообщества по ID сообщества
export const projektojQuerySimple = gql`
  query Projektoj(
    $komunumoId: Float
    $first: Int
    $forigo: Boolean = false
    $arkivo: Boolean = false
    $after: String
  ) {
    projektojSimple: projektojProjekto(
      first: $first
      projektojprojektoposedanto_PosedantoKomunumo_Id: $komunumoId
      arkivo: $arkivo
      forigo: $forigo
      after: $after
      orderBy: ["-krea_dato"]
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          objId
          nomo {
            enhavo
          }
        }
      }
    }
  }
`
