import gql from 'graphql-tag'
// добавление проекта
export const addProjekto = gql`
  mutation addProjekto(
    $publikigo: Boolean = true
    $nomo: String!
    $kategorio: [Int] = 1
    $tipoId: Int = 1
    $statusoId: Int = 1
    $priskribo: String!
    $posedantoUzantoId: Int!
    $posedantoKomunumoId: Int!
    $posedantoTipoId: Int = 1
    $posedantoStatusoId: Int = 1
    $uuid: UUID
  ) {
    redaktuProjektojProjekto(
      publikigo: $publikigo
      nomo: $nomo
      kategorio: $kategorio
      tipoId: $tipoId
      statusoId: $statusoId
      priskribo: $priskribo
      posedantoUzantoId: $posedantoUzantoId
      posedantoKomunumoId: $posedantoKomunumoId
      posedantoTipoId: $posedantoTipoId
      posedantoStatusoId: $posedantoStatusoId
      uuid: $uuid
    ) {
      status
      message
      universoProjekto {
        uuid
        id
        objId
        nomo {
          enhavo
        }
        priskribo {
          enhavo
        }
        tipo {
          nomo {
            enhavo
          }
        }
        pozicio
        realeco {
          nomo {
            enhavo
          }
        }
        kategorio {
          edges {
            node {
              nomo {
                enhavo
              }
            }
          }
        }
        statuso {
          nomo {
            enhavo
          }
        }
        objekto {
          nomo {
            enhavo
          }
        }
        projektojprojektoposedantoSet {
          edges {
            node {
              projekto {
                nomo {
                  enhavo
                }
              }
            }
          }
        }
        tasko {
          edges {
            node {
              id
              nomo {
                enhavo
              }
              priskribo {
                enhavo
              }
            }
          }
        }
      }
    }
  }
`
// регистрация пользователя Универсо
export const addUniversoUzanto = gql`
  mutation ($retnomo: String!) {
    redaktuUniversoUzanto(retnomo: $retnomo, publikigo: true) {
      status
      message
    }
  }
`
// удаление проекта
export const forigoProjekto = gql`
  mutation forigoProjekto($id: UUID, $forigo: Boolean = true) {
    redaktuProjektojProjekto(uuid: $id, forigo: $forigo) {
      status
      message
    }
  }
`
// архивация проекта
export const arkProjekto = gql`
  mutation arkProjekto($id: UUID, $arkivo: Boolean = true) {
    redaktuProjektojProjekto(uuid: $id, arkivo: $arkivo) {
      status
      message
    }
  }
`
// добавление задачи
export const addTasko = gql`
  mutation addTasko(
    $publikigo: Boolean = true
    $nomo: String!
    $kategorio: [Int] = 1
    $tipoId: Int = 1
    $statusoId: Int = 1
    $priskribo: String!
    $projektoUuid: String
  ) {
    redaktuProjektojTaskoj(
      publikigo: $publikigo
      nomo: $nomo
      kategorio: $kategorio
      tipoId: $tipoId
      statusoId: $statusoId
      priskribo: $priskribo
      projektoUuid: $projektoUuid
    ) {
      status
      message
      universoTaskoj {
        uuid
        id
        objId
        nomo {
          enhavo
        }
        priskribo {
          enhavo
        }
        kategorio {
          edges {
            node {
              objId
              nomo {
                enhavo
              }
            }
          }
        }
        tipo {
          objId
          nomo {
            enhavo
          }
        }
        statuso {
          objId
          nomo {
            enhavo
          }
        }
      }
    }
  }
`
// создание категории задач
export const addTaskojKategorio = gql`
  mutation addTaskojKategorio($publikigo: Boolean = true, $nomo: String!, $realeco: [Int] = 1) {
    redaktuProjektojTaskojKategorio(publikigo: $publikigo, nomo: $nomo, realeco: $realeco) {
      status
      message
      universoTaskojKategorioj {
        uuid
        id
        objId
      }
    }
  }
`
// создание типов задач
export const addTaskojTipo = gql`
  mutation addTaskojTipo($publikigo: Boolean = true, $nomo: String!, $realeco: [Int] = 1) {
    redaktuProjektojTaskojTipo(publikigo: $publikigo, nomo: $nomo, realeco: $realeco) {
      status
      message
      universoTaskojTipoj {
        uuid
        id
        objId
      }
    }
  }
`
// создание статусов задач
export const addTaskojStatuso = gql`
  mutation addTaskojStatuso($publikigo: Boolean = true, $nomo: String!) {
    redaktuProjektojTaskoStatuso(publikigo: $publikigo, nomo: $nomo) {
      status
      message
      universoTaskojStatusoj {
        uuid
        id
        objId
      }
    }
  }
`
// архивация задачи
export const arkTasko = gql`
  mutation arkTasko($id: UUID, $arkivo: Boolean = true) {
    redaktuProjektojTaskoj(uuid: $id, arkivo: $arkivo) {
      status
      message
    }
  }
`
// удаление задачи
export const forigoTasko = gql`
  mutation arkTasko($id: UUID, $forigo: Boolean = true) {
    redaktuProjektojTaskoj(uuid: $id, forigo: $forigo) {
      status
      message
    }
  }
`
// логин
export const login_query = gql`
  mutation ($login: String!, $password: String!) {
    ensaluti(login: $login, password: $password) {
      status
      message
      konfirmita
    }
  }
`
// логаут
export const logout_query = gql`
  mutation {
    elsaluti {
      status
      message
    }
  }
`
// изменение пароля
export const changePassword = gql`
  mutation shanghiPasvorton($uzantoId: Int!, $pasvorto: String!, $novaPasvorto: String!) {
    shanghiPasvorton(uzantoId: $uzantoId, pasvorto: $pasvorto, novaPasvorto: $novaPasvorto) {
      status
      message
      csrfToken
    }
  }
`
// изменение настроек профиля пользователя
export const changeSettings = gql`
  mutation aplikiAgordoj(
    $avataro: String
    $chefaOrganizo: Int
    $duaNomo: String
    $familinomo: String
    $id: Int
    $kovrilo: String
    $lando: String
    $lingvo: String
    $naskighdato: Date
    $poshtoSciigoj: Boolean
    $regiono: String
    $sekso: String
    $statuso: String
    $unuaNomo: String
  ) {
    aplikiAgordoj(
      unuaNomo: $unuaNomo
      duaNomo: $duaNomo
      familinomo: $familinomo
      avataro: $avataro
      chefaOrganizo: $chefaOrganizo
      kovrilo: $kovrilo
      id: $id
      lando: $lando
      lingvo: $lingvo
      naskighdato: $naskighdato
      poshtoSciigoj: $poshtoSciigoj
      regiono: $regiono
      sekso: $sekso
      statuso: $statuso
    ) {
      status
      message
      errors {
        field
        message
      }
      poshtoSciigoj
      unuaNomo {
        lingvo
        enhavo
        chefaVarianto
      }
      duaNomo {
        lingvo
        enhavo
        chefaVarianto
      }
      familinomo {
        lingvo
        enhavo
        chefaVarianto
      }
      sekso
      statuso {
        lingvo
        enhavo
        chefaVarianto
      }
      naskighdato
      lingvo {
        id
      }
      lando {
        id
      }
      regiono {
        id
      }
      chefaOrganizo {
        id
      }
      avataro {
        id
      }
      kovrilo {
        id
      }
    }
  }
`
// изменение проекта
export const changeProjekto = gql`
  mutation changeProjekto(
    $publikigo: Boolean
    $nomo: String
    $kategorio: [Int]
    $tipoId: Int
    $statusoId: Int
    $priskribo: String
    $posedantoUzantoId: Int
    $posedantoKomunumoId: Int
    $posedantoTipoId: Int
    $posedantoStatusoId: Int
    $uuid: UUID!
  ) {
    redaktuProjektojProjekto(
      publikigo: $publikigo
      nomo: $nomo
      kategorio: $kategorio
      tipoId: $tipoId
      statusoId: $statusoId
      priskribo: $priskribo
      posedantoUzantoId: $posedantoUzantoId
      posedantoKomunumoId: $posedantoKomunumoId
      posedantoTipoId: $posedantoTipoId
      posedantoStatusoId: $posedantoStatusoId
      uuid: $uuid
    ) {
      status
      message
      universoProjekto {
        uuid
        id
        objId
        nomo {
          enhavo
        }
        priskribo {
          enhavo
        }
        tipo {
          nomo {
            enhavo
          }
        }
        pozicio
        realeco {
          nomo {
            enhavo
          }
        }
        kategorio {
          edges {
            node {
              nomo {
                enhavo
              }
            }
          }
        }
        statuso {
          nomo {
            enhavo
          }
        }
        objekto {
          nomo {
            enhavo
          }
        }
        projektojprojektoposedantoSet {
          edges {
            node {
              projekto {
                nomo {
                  enhavo
                }
              }
            }
          }
        }
        tasko {
          edges {
            node {
              uuid
              id
              objId
              nomo {
                enhavo
              }
              priskribo {
                enhavo
              }
              kategorio {
                edges {
                  node {
                    objId
                    nomo {
                      enhavo
                    }
                  }
                }
              }
              tipo {
                objId
                nomo {
                  enhavo
                }
              }
              statuso {
                objId
                nomo {
                  enhavo
                }
              }
            }
          }
        }
      }
    }
  }
`
// изменение задачи
export const changeTasko = gql`
  mutation changeTasko(
    $publikigo: Boolean
    $nomo: String
    $kategorio: [Int]
    $tipoId: Int
    $statusoId: Int
    $priskribo: String
    $posedantoUzantoId: Int
    $posedantoTipoId: Int
    $posedantoStatusoId: Int
    $uuid: UUID!
  ) {
    redaktuProjektojTaskoj(
      publikigo: $publikigo
      nomo: $nomo
      kategorio: $kategorio
      tipoId: $tipoId
      statusoId: $statusoId
      priskribo: $priskribo
      posedantoUzantoId: $posedantoUzantoId
      posedantoTipoId: $posedantoTipoId
      posedantoStatusoId: $posedantoStatusoId
      uuid: $uuid
    ) {
      status
      message
      universoTaskoj {
        objId
        nomo {
          enhavo
        }
        priskribo {
          enhavo
        }
        uuid
        kategorio {
          edges {
            node {
              objId
              id
              nomo {
                enhavo
              }
            }
          }
        }
        statuso {
          objId
          id
          nomo {
            enhavo
          }
        }
        tipo {
          objId
          id
          nomo {
            enhavo
          }
        }
        forigo
        forigaDato
        arkivo
        arkivaDato
      }
    }
  }
`
// изменение телефона или электронной почты
export const changeTelephoneNumberOrEmail = gql`
  mutation shanghiPoshtonTelefonon($tel: String, $code: String, $email: String) {
    shanghiPoshtonTelefonon(telefonaNombro: $tel, konfirmaKodo: $code, retposhto: $email) {
      status
      message
    }
  }
`
// изменение аватары
export const changeAvatar = gql`
  mutation instaliAvataron($id: Int!, $baza: String, $full: String, $min: String) {
    instaliAvataron(uzantoId: $id, bildoBaza: $baza, bildoAvataro: $full, bildoAvataroMin: $min) {
      status
      message
      errors {
        field
        message
      }
      avataro {
        bildoE {
          url
        }
        bildoF {
          url
        }
      }
    }
  }
`
// изменение или создание связей проектов
export const changeLigilo = gql`
  mutation ligilo(
    $uuid: UUID
    $tipoId: Int
    $arkivo: Boolean
    $forigo: Boolean
    $ligiloId: Int
    $posedantoId: Int
    $publikigo: Boolean
  ) {
    redaktuProjektojProjektoLigilo(
      uuid: $uuid
      tipoId: $tipoId
      arkivo: $arkivo
      forigo: $forigo
      ligiloId: $ligiloId
      posedantoId: $posedantoId
      publikigo: $publikigo
    ) {
      status
      message
      universoProjektoLigilo {
        uuid
        posedanto {
          objId
          id
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
        }
        ligilo {
          objId
          id
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
        }
        tipo {
          id
          uuid
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
        }
        id
      }
    }
  }
`
// вступление в сообщество
export const kuniguKomunumon = gql`
  mutation kuniguKomunumon($komunumoUuid: UUID!) {
    kuniguKomunumon(komunumoUuid: $komunumoUuid) {
      status
      message
      statistiko {
        mia
      }
    }
  }
`
// выход из сообщества
export const forlasuKomunumon = gql`
  mutation forlasuKomunumon($komunumoUuid: UUID!) {
    forlasuKomunumon(komunumoUuid: $komunumoUuid) {
      status
      message
      statistiko {
        mia
      }
    }
  }
`
