export default {
  // Cюда добавлять url страниц
  komunumo: { path: '^(?P<kom_langeto>idk|inf|frm)(?P<kom_id>\\d+)$', com: 'kom/idk' },
  uzanto: { path: '^idu(?P<uzanto_id>\\d+)$', com: 'uzanto/idu' },
  konferenco: { path: '^konf(?P<konferenco_id>\\d+)$', com: 'Komunumo/Konferenco/Konferenco' },
  batalanto: { path: '^batl(?P<batalanto_id>\\d+)$', com: 'Komunumo/Laboro/Batalanto' },
  batalantoListo: { path: '^(?P<batlist>)$', com: 'Komunumo/Laboro/BatalantoListo' }
}
