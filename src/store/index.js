import knowledgeBase from './knowledgeBase'
import siriusoauth from './siriusoauth'
import komunumoj from './komunumoj'
import komunumo from './komunumo'
import projektoj from './projektoj'
import tutaProjektoj from './tutaProjektoj'
import uzantoj from './uzantoj'
import gekamaradoj from './gekamaradoj'
import uzanto from './uzanto'
import projekto from './projekto'
import taskoj from './taskoj'
import tasko from './tasko'
import UIstore from './UIstore'
import registrado from './registrado'
import ligoj from './ligoj'
import test from './test'

// default src/store/index.js content:
import { createStore } from 'vuex'
// import example from './module-example'

export default function (/* { ssrContext } */) {
  const Store = createStore({
    modules: {
      knowledgeBase,
      siriusoauth,
      komunumoj,
      komunumo,
      projektoj,
      tutaProjektoj,
      uzantoj,
      gekamaradoj,
      uzanto,
      projekto,
      taskoj,
      UIstore,
      tasko,
      registrado,
      ligoj,
      test
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING
  })

  return Store
}

// import Vue from "vue";
// import Vuex from "vuex";
// import knowledgeBase from "./knowledgeBase";
// import siriusoauth from "./siriusoauth";
// import komunumoj from "./komunumoj";
// import komunumo from "./komunumo";
// import projektoj from "./projektoj";
// import tutaProjektoj from "./tutaProjektoj";
// import uzantoj from "./uzantoj";
// import gekamaradoj from "./gekamaradoj";
// import uzanto from "./uzanto";
// import projekto from "./projekto";
// import taskoj from "./taskoj";
// import tasko from "./tasko";
// import UIstore from "./UIstore";
// import registrado from "./registrado";
// import ligoj from './ligoj';
// import test from'./test'
//
//
// // import example from './module-example'
//
// Vue.use(Vuex);
//
// /*
//  * If not building with SSR mode, you can
//  * directly export the Store instantiation;
//  *
//  * The function below can be async too; either use
//  * async/await or return a Promise which resolves
//  * with the Store instance.
//  */
//
// export default function(/* { ssrContext } */) {
//   const Store = new Vuex.Store({
//     modules: {
//       knowledgeBase,
//       siriusoauth,
//       komunumoj,
//       komunumo,
//       projektoj,
//       tutaProjektoj,
//       uzantoj,
//       gekamaradoj,
//       uzanto,
//       projekto,
//       taskoj,
//       UIstore,
//       tasko,
//       registrado,
//       ligoj,
//       test
//
//       // example
//     },
//
//     // enable strict mode (adds overhead!)
//     // for dev mode only
//     strict: process.env.DEBUGGING
//   });
//   // if (process.env.DEV && module.hot) {
//   //   module.hot.accept(['./knowledgeBase'], () => {
//   //     const newKnowledgeBase = require('./knowledgeBase').default
//   //     Store.hotUpdate({modules: {knowledgeBase: newKnowledgeBase}})
//   //   })
//   // }
//   if (process.env.DEV && module.hot) {
//     module.hot.accept(["./siriusoauth"], () => {
//       const newSiriusoauth = require("./siriusoauth").default;
//       Store.hotUpdate({ modules: { siriusoauth: newSiriusoauth } });
//     });
//   }
//
//   return Store;
// }
