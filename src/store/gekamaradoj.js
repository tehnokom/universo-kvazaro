import graphqlClient from '../utils/client'
import { gekamaradojQuery } from 'src/queries/queries'

export default {
  namespaced: true,
  state() {
    return {
      list: null,
      edges: [],
      pageInfo: null
    }
  },
  getters: {
    getGekamaradoj: (state) => {
      return {
        pageInfo: state.pageInfo,
        edges: state.edges.map((item) => item[1])
      }
    }
  },
  mutations: {
    setGekamaradoj: (state, payload) => {
      let mapList
      mapList = new Map(state.edges)
      payload.gekamaradoj.edges.forEach((item) => {
        mapList.set(item.node.uuid, item)
      })
      state.pageInfo = payload.gekamaradoj.pageInfo
      state.edges = Array.from(mapList)
    }
  },
  actions: {
    fetchGekamaradoj({ commit }, { first, after }) {
      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .query({
          query: gekamaradojQuery,
          variables: { first, after },
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setGekamaradoj', data)
          this.dispatch('UIstore/hideLoading')
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading')
        })
    },
    refetchGekamaradoj({ commit }, { first, after }) {
      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .query({
          query: gekamaradojQuery,
          variables: { first, after },
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setGekamaradoj', data)
          this.dispatch('UIstore/hideLoading')
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading')
        })
    }
  }
}
