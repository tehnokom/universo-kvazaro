export default {
  namespaced: true,
  state() {
    return {
      isLoading: 0,
      dark: true,
      stateUI: null,
      showLoginForm: false
    }
  },
  getters: {
    getIsLoading: (state) => state.isLoading > 0,
    getDark: (state) => state.dark,
    getStateUI: (state) => state.stateUI,
    getShowLoginForm: (state) => state.showLoginForm
  },
  mutations: {
    showLoginForm(state, payload) {
      state.showLoginForm = payload
    },
    showIsLoading(state) {
      state.isLoading++
    },
    hideIsLoading(state) {
      state.isLoading !== 0 ? state.isLoading-- : (state.isLoading = 0)
    },
    setDark(state, payload) {
      state.dark = payload
      localStorage.setItem('dark', payload)
    },
    setStateUI(state, payload) {
      state.stateUI = Object.assign({}, state.stateUI, payload)
      localStorage.setItem('stateUI', JSON.stringify(state.stateUI))
    },
    resetIsLoading(state) {
      state.isLoading = 0
    }
  },
  actions: {
    showLoading(store) {
      store.commit('showIsLoading')
      setTimeout(() => {
        store.commit('resetIsLoading')
      }, 15000)
    },
    hideLoading(store) {
      setTimeout(() => {
        store.commit('hideIsLoading')
      }, 500)
    }
  }
}
