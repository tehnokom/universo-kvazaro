import graphqlClient from '../utils/client'
import { komunumoQuery } from 'src/queries/queries'
import { forlasuKomunumon, kuniguKomunumon } from 'src/queries/mutations'

export default {
  namespaced: true,
  state() {
    return {
      item: null
    }
  },
  getters: {
    getKomunumo: (state) => {
      return state.item
    }
  },
  mutations: {
    setKomunumo: (state, payload) => {
      state.item = payload
    },
    // устанавливаем состояние подписки/отписки у сообщества
    changeKomunumon: (state, payload) => {
      state.item.statistiko.mia = payload.mia
    }
  },
  actions: {
    fetchKomunumo({ commit }, { id }) {
      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .query({
          query: komunumoQuery,
          variables: { id },
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          if (data.komunumo) {
            commit('setKomunumo', data.komunumo.edges[0].node)
          }
          this.dispatch('UIstore/hideLoading')
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading')
        })
    },
    membershipKomunumon: function ({ commit }, payload) {
      const mutation = payload.joinOrLeave ? kuniguKomunumon : forlasuKomunumon
      const mutationName = payload.joinOrLeave ? 'kuniguKomunumon' : 'forlasuKomunumon'

      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .mutate({
          mutation: mutation,
          variables: { komunumoUuid: payload.komunumoUuid },
          errorPolicy: 'all',
          update: () => {
            console.log('Вносим изменение')
          }
        })
        .then(({ data }) => {
          this.dispatch('UIstore/hideLoading')
          if (data[mutationName].status) {
            const params = {
              mia: data[mutationName].statistiko.mia
            }
            commit('changeKomunumon', params)
            return Promise.resolve(data[mutationName])
          } else return Promise.reject(data[mutationName])
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading')
          console.error(err)
        })
    }
  }
}
