// import something here
import dateFilter from 'src/utils/filters/date.filter'
import objIdFilter from 'src/utils/filters/objId.filter'
import urlFilter from 'src/utils/filters/url.filter'

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
// export default async (/* { app, router, Vue ... } */) => {
// something to do
// }
export default ({ Vue }) => {
  Vue.filter('date', dateFilter)
  Vue.filter('objId', objIdFilter)
  Vue.filter('sanitizeurl', urlFilter)
}
