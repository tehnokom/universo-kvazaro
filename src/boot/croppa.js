import { boot } from 'quasar/wrappers'
import Croppa from 'vue-croppa'
import 'vue-croppa/dist/vue-croppa.css'

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(async ({ app }) => {
  app.use(Croppa)
})

//
// import Vue from 'vue';
// import Croppa from 'vue-croppa';
// // If your build tool supports css module
// import 'vue-croppa/dist/vue-croppa.css';
// Vue.use(Croppa);
