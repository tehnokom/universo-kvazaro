import { boot } from 'quasar/wrappers'
import { VueReCaptcha, useReCaptcha } from 'vue-recaptcha-v3'

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(async ({ app }) => {
  //   const component = {
  //   setup() {
  //     const { executeRecaptcha, recaptchaLoaded } = useReCaptcha()
  //
  //     const recaptcha = async () => {
  //       // (optional) Wait until recaptcha has been loaded.
  //       await recaptchaLoaded()
  //
  //       // Execute reCAPTCHA with action "login".
  //       const token = await executeRecaptcha('login')
  //
  //       // Do stuff with the received token.
  //     }
  //
  //     return {
  //       recaptcha
  //     }
  //   },
  //   template: '<button @click="recaptcha">Execute recaptcha</button>'
  // }
  //
  //
  // app.use(VueReCaptcha , { siteKey: process.env.VUE_APP_RECAPTCHA_SITE_KEY || '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI', })
})

// import { createApp } from 'vue'
// import { VueReCaptcha, useReCaptcha } from 'vue-recaptcha-v3'
//
// const component = {
//   setup() {
//     const { executeRecaptcha, recaptchaLoaded } = useReCaptcha()
//
//     const recaptcha = async () => {
//       // (optional) Wait until recaptcha has been loaded.
//       await recaptchaLoaded()
//
//       // Execute reCAPTCHA with action "login".
//       const token = await executeRecaptcha('login')
//
//       // Do stuff with the received token.
//     }
//
//     return {
//       recaptcha
//     }
//   },
//   template: '<button @click="recaptcha">Execute recaptcha</button>'
// }
//
// createApp(component)
// .use(VueReCaptcha, { siteKey: '<site key>' })
