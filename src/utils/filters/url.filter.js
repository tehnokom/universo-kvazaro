import { sanitizeUrl } from '@braintree/sanitize-url'

export default function urlFilter(value) {
  return sanitizeUrl(value)
}
