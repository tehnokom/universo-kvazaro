export function getCookie(name) {
  const matches = document.cookie.match(
    new RegExp('(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)')
  )
  return matches ? decodeURIComponent(matches[1]) : undefined
}

export const confirmDialogParams = {
  title: 'Подтвердите действие',
  ok: {
    color: 'positive',
    label: 'Да'
  },
  cancel: {
    color: 'negative',
    label: 'Нет'
  },
  persistent: true,
  dark: true,
  transitionShow: 'slide-down',
  transitionHide: 'slide-down',
  style: 'max-width: 100%'
}

export function isJson(item) {
  item = typeof item !== 'string' ? JSON.stringify(item) : item
  try {
    item = JSON.parse(item)
  } catch (e) {
    return false
  }
  if (typeof item === 'object' && item !== null) return true
  return false
}

export function itemPriscriboContent(item) {
  if (!isJson(item)) return item
  else {
    if (JSON.parse(item).content.length > 0) {
      let str = ''
      JSON.parse(item).content.forEach((k, i) => {
        // if (i === 0) return;
        try {
          const oneString = k.content
            .map((val) => {
              return val.type === 'text' ? val.text.trim() : ''
            })
            .join(' ')
          str = str + oneString + '\n'
        } catch (e) {}
      })
      return str
    } else return ''
  }
}

export function idParser(path, id, number) {
  if (!id && !number) return

  let idToString = ''
  if (this.$route.params.hasOwnProperty(id)) {
    idToString = this.$route.params[id].toString()
  } else {
    // console.log('no property id' );
    // console.log('number is: ',number);
  }

  const str = id ? idToString : number.toString()
  const matched = str.match(/[0-9]+/)
  if (matched) {
    if (matched[0] === str) return +str
  }

  if (path) {
    this.$router.push(path)
    requestAnimationFrame(() => this.$store.commit('UIstore/resetIsLoading'))
  }
}

export function yandexBanner() {
  ;(function (w, d, n, s, t) {
    w[n] = w[n] || []
    w[n].push(function () {
      Ya.Context.AdvManager.render(
        {
          blockId: 'R-A-945396-1',
          renderTo: 'yandex_rtb_R-A-945396-1',
          async: true,
          onRender: function (data) {
            // console.log(data.product);
          }
        },
        function altCallback() {
          // код вызова своей рекламы в блоке
        }
      )
    })
    t = d.getElementsByTagName('script')[0]
    s = d.createElement('script')
    s.type = 'text/javascript'
    s.src = '//an.yandex.ru/system/context.js'
    s.async = true
    t.parentNode.insertBefore(s, t)
  })(window, document, 'yandexContextAsyncCallbacks')
}

export function yandexMetrika() {
  //* *****************Метрика Yandex**********************
  // var fired = false;
  //
  // window.addEventListener('scroll', () => {
  //   if (fired === false) {
  //     fired = true;
  //
  //     setTimeout(() => {
  ;(function (m, e, t, r, i, k, a) {
    m[i] =
      m[i] ||
      function () {
        ;(m[i].a = m[i].a || []).push(arguments)
      }
    m[i].l = 1 * new Date()
    k = e.createElement(t)
    a = e.getElementsByTagName(t)[0]
    k.async = 1
    k.src = r
    a.parentNode.insertBefore(k, a)
  })(window, document, 'script', 'https://mc.yandex.ru/metrika/tag.js', 'ym')

  ym(69639250, 'init', {
    clickmap: true,
    trackLinks: true,
    accurateTrackBounce: true
  })
  //     }, 1000)
  //   }
  // });
  //* *****************Метрика Yandex**********************
}
