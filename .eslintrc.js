module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true
  },
  extends: [
    //
    'plugin:vue/vue3-essential',
    'standard',
    'prettier'
  ],
  globals: {
    ga: true, // Google Analytics
    Ya: true,
    ym: true,
    cordova: true,
    __statics: true,
    process: true,
    Capacitor: true,
    chrome: true
  },
  parserOptions: {
    ecmaVersion: 2018,
    parser: '@typescript-eslint/parser',
    sourceType: 'module'
  },
  plugins: [
    //
    'vue',
    'quasar',
    'node',
    '@typescript-eslint',
    'prettier'
  ],
  rules: {
    // TODO warn -> error & fix
    camelcase: 'warn',
    'prefer-const': 'warn',
    'no-empty-pattern': 'warn',
    'no-prototype-builtins': 'warn',
    'no-unused-expressions': 'warn',
    'no-unused-vars': 'warn',
    'no-useless-escape': 'warn',
    'no-void': 'warn',
    'handle-callback-err': 'warn',
    'node/handle-callback-err': 'warn',
    'vue/no-deprecated-v-on-native-modifier': 'warn',
    'vue/no-mutating-props': 'warn',

    'no-console': 'warn',
    'prefer-promise-reject-errors': 'error',

    'prettier/prettier': ['error', {}, {}],

    // TODO allow debugger during development only
    // "no-debugger": process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': 'error'
  },
  root: true
}
